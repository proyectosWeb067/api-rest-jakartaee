/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.excepciones;

/**
 *
 * @author yio
 */
public class MensajeError {
    
    private String mensaje;
    private int codigo;
    private String descripcion;
    
    
    public MensajeError(){
    }
    
    public MensajeError(String mensaje, int codigo, String descripcion){
        this.mensaje = mensaje;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
