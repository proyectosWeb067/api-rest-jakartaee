/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.excepciones;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author yio
 *///excepcion generica para tratar los errores url(mal escrito el EndPoint)
@Provider // importatne esta anotacion para generar la excepcion
public class ExcepcionGenerica implements ExceptionMapper<Throwable>{

    @Override
    public Response toResponse(Throwable e) {
        MensajeError mensaje = new MensajeError(e.getMessage(), 500, e.getCause().toString());
        return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
    }
    
}
