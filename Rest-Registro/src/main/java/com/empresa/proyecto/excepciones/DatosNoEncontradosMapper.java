/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.excepciones;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author yio
 */
//Mensaje de error personalizado
@Provider //esta anotacion es necesaria
public class DatosNoEncontradosMapper implements ExceptionMapper<DatosNoEncontradosExcepcion>{

    @Override
    public Response toResponse(DatosNoEncontradosExcepcion e) {
        MensajeError mensaje = new MensajeError("Datos no encontrados", 404,"Informacion no encontrada");
        return Response.status(Response.Status.NOT_FOUND).entity(mensaje).build();
    }
    
}
