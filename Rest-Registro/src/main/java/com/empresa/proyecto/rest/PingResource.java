package com.empresa.proyecto.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author airhacks.com
 */

//para acceder rest/ping ()
@Path("ping")
@Produces("application/json")
@Consumes("application/json")
public class PingResource {

    @GET
    public String ping() {
        return "Enjoy Java EE 8!";
    }

}
