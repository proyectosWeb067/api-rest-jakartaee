/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.rest;

import com.empresa.proyecto.controller.ProductoFacade;
import com.empresa.proyecto.excepciones.DatosNoEncontradosExcepcion;
import com.empresa.proyecto.model.Producto;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author yio
 */
@Path("productos")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")
public class ProductoEndpoints {

    @EJB
    ProductoFacade productoService;

    @GET
    public List<Producto> listAll() {

        return productoService.findAll();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}") //exprecion regular
    public Producto findProductoById(@PathParam("id") int id) {
        Producto p = productoService.find(id);
        if (p == null) {
            throw new DatosNoEncontradosExcepcion("No se encuentro el usuario");
        }
        return p;
    }

    @POST
    @Path("/{nom:[a-zA-Z\\s]+}")
    @Produces(MediaType.TEXT_PLAIN)
    public String saveProducto(@PathParam("nom") String nom, Producto p) {
        boolean prodEncontrado;
        String msj;
        prodEncontrado = productoService.findProducto(nom);
        if (prodEncontrado == true) {
            msj = "El producto ya existe, cree uno diferente";
            return msj;
        } else {
            productoService.create(p);
            msj = "Se creo un producto";
            return msj;
        }
    }

    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.TEXT_PLAIN)
    public String updateProducto(@PathParam("id") int id, Producto pro) {
        String msj;
        Producto p = productoService.find(id);
        if (p != null) {
            productoService.edit(pro);
        }
        msj = "Usuario modificado";
        return msj;
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteProducto(@PathParam("id") int id) {
        Producto p = productoService.find(id);
        System.out.println("prducto-->"+p.getTitulo());
        String msj;
        if (p != null) {
            productoService.remove(p);
            msj = "Producto eliminado";
            return msj;
        }
        msj = "No fue posible eliminar el producto";
        return msj;
    }
}
