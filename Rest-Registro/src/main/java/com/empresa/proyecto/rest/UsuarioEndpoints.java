/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.rest;

import com.empresa.proyecto.controller.UsuarioFacade;
import com.empresa.proyecto.excepciones.DatosNoEncontradosExcepcion;
import com.empresa.proyecto.model.Usuario;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;

import java.util.*;
import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author yio
 */
// lo que quiero es que este servicio rest sea consumido por una spa(
@Path("usuarios")
@RequestScoped   //es una buena practica colcar en los endpoint el scooped, es decir cual seria el ciclo de vida de este componente
@Produces("application/json") //tambien en cada metodo con la antotacion @Produces(MediaType.APPLICATION_XML O APPLICATION_JSON)
@Consumes("application/json")  // los puedo colocar a nivel global para que aplique a todos los metodos o a nivel local 
public class UsuarioEndpoints {

    @EJB //INYECTADO UN EJB
    UsuarioFacade usuarioService;

    @GET
    //@Produces(MediaType.APPLICATION_XML) ---> utilizando jaxb atraves de la anotacion(@XmlRootElement) en la entity para generar un objeto en xml
    public List<Usuario> listAll() {
        return usuarioService.findAll();
    }

    @GET
    @Path("/{id:[0-9][0-9]*}")//esto seria por url y haria match con el @Pathparam del metodo, exprecion regular cualquier numero de 0-9 1 o mas veces
    public Usuario findById(@PathParam("id") int id) {

        Usuario u;
        u = usuarioService.find(id);
        if (u == null) {
            throw new DatosNoEncontradosExcepcion("No se encontraron resultados");
        }
        return u;
    }

    @POST //Response me permite generar una respuesta y establecer el codigo y los encabezados de la respuesta en http
    @Path("/{numdoc:[0-9][0-9]*}")
    @Produces(MediaType.TEXT_PLAIN)
    public String create(@PathParam("numdoc") int numdoc, Usuario u) {

        String mens;
        boolean nuDoc;
        nuDoc = usuarioService.findUsuario(numdoc);
        if (nuDoc == false && u != null) {
            usuarioService.create(u);
            mens = "usuario agregado";
            return mens;
        } else {
            //me devuelve en donde se encuentra el recurso, en la info de la cabecera.
            //return Response.created(UriBuilder.fromResource(UsuarioEndpoints.class).path(String.valueOf(u.getIdusuario())).build()).build();
            mens = "Ya existe un usuario con este documento";
            return mens;
        }
    }

    @PUT //actualizar a partir de un identificador
    @Path("/{id:[0-9][0-9]*}")
    public Response update(@PathParam("id") int id, Usuario u) {
        usuarioService.edit(u);

        //me devulve el articulo creado
        return Response.status(Response.Status.CREATED).entity(u).build();
    }

    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    @Produces(MediaType.TEXT_PLAIN)
    public String delete(@PathParam("id") int id) {
       Usuario u = usuarioService.find(id); 
        String msj;
        if (u != null) {
            usuarioService.remove(u);
            msj = "Usuario eliminado";
            return msj;
        }
        msj = "No fue posible eliminar el usuario";
        return msj;
         
        //return Response.noContent().build();//para retornar codigo 200, me indica que la api esta habilitada
       //return Response.noContent().build();
    }
}
