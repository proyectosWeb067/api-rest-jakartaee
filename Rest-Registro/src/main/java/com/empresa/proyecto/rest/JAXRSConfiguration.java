package com.empresa.proyecto.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author airhacks.com
 */

//ruta base para todas nuestras apis,se podria describir como un activador
@ApplicationPath("rest")
public class JAXRSConfiguration extends Application {

}
