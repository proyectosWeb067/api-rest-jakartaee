/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.controller;

import com.empresa.proyecto.model.Producto;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author yio
 */
@Stateless

public class ProductoFacade extends AbstractFacade<Producto> {

    @PersistenceContext(unitName = "restapi_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductoFacade() {
        super(Producto.class);
    }

    //verificando si existe un producto con un nombre determinado
    public boolean findProducto(String nom) {
        Producto p;
        String jpql = "Producto.findByTitulo";
        Query q = em.createNamedQuery(jpql);

        try {
            p = (Producto) q.getSingleResult();
            if (p != null) {
                return true;
            }
        } catch (Exception e) {
            e.getMessage();
        }

        return false;
    }
}
