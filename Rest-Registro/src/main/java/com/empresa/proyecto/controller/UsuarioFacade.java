/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.controller;

import com.empresa.proyecto.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author yio
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "restapi_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    //metodo para verificar si el numero documento ya esta registrado
    public boolean findUsuario(int numdoc) {
        String jpql = "Usuario.findByNumdoc";

        Query query = em.createNamedQuery(jpql);
         Usuario u = (Usuario) query.getSingleResult();
         if(u != null){
             return true;
         }
        /*if (query.getResultList().size() > 0) {
            return true;
        }*/
        return false;
    }
     

}
