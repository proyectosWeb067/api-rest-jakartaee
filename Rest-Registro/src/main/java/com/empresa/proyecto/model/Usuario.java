/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author yio
 */
//@XmlRootElement ------> esta anotacion permite que la entity genere xml cuando se consume el ws
@Entity
@Table(name = "usuario")
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByIdusuario", query = "SELECT u FROM Usuario u WHERE u.idusuario = :idusuario"),
    @NamedQuery(name = "Usuario.findByNombre", query = "SELECT u FROM Usuario u WHERE u.nombre = :nombre"),
    @NamedQuery(name = "Usuario.findByApellpat", query = "SELECT u FROM Usuario u WHERE u.apellpat = :apellpat"),
    @NamedQuery(name = "Usuario.findByApellmat", query = "SELECT u FROM Usuario u WHERE u.apellmat = :apellmat"),
    @NamedQuery(name = "Usuario.findByTipodoc", query = "SELECT u FROM Usuario u WHERE u.tipodoc = :tipodoc"),
    @NamedQuery(name = "Usuario.findByNumdoc", query = "SELECT u FROM Usuario u WHERE u.numdoc = :numdoc"),
    @NamedQuery(name = "Usuario.findByDianac", query = "SELECT u FROM Usuario u WHERE u.dianac = :dianac"),
    @NamedQuery(name = "Usuario.findByMesnac", query = "SELECT u FROM Usuario u WHERE u.mesnac = :mesnac"),
    @NamedQuery(name = "Usuario.findByAnac", query = "SELECT u FROM Usuario u WHERE u.anac = :anac"),
    @NamedQuery(name = "Usuario.findBySexo", query = "SELECT u FROM Usuario u WHERE u.sexo = :sexo"),
    @NamedQuery(name = "Usuario.findByRuc", query = "SELECT u FROM Usuario u WHERE u.ruc = :ruc"),
    @NamedQuery(name = "Usuario.findByEstadcivil", query = "SELECT u FROM Usuario u WHERE u.estadcivil = :estadcivil"),
    @NamedQuery(name = "Usuario.findByEstadolabo", query = "SELECT u FROM Usuario u WHERE u.estadolabo = :estadolabo")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idusuario")
    private Integer idusuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "apellpat")
    private String apellpat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "apellmat")
    private String apellmat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "tipodoc")
    private String tipodoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numdoc")
    private int numdoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dianac")
    private int dianac;
    @Size(max = 30)
    @Column(name = "mesnac")
    private String mesnac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anac")
    private int anac;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "sexo")
    private String sexo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ruc")
    private int ruc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "estadcivil")
    private String estadcivil;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "estadolabo")
    private String estadolabo;

    public Usuario() {
    }

    public Usuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public Usuario(Integer idusuario, String nombre, String apellpat, String apellmat, String tipodoc, int numdoc, int dianac, int anac, String sexo, int ruc, String estadcivil, String estadolabo) {
        this.idusuario = idusuario;
        this.nombre = nombre;
        this.apellpat = apellpat;
        this.apellmat = apellmat;
        this.tipodoc = tipodoc;
        this.numdoc = numdoc;
        this.dianac = dianac;
        this.anac = anac;
        this.sexo = sexo;
        this.ruc = ruc;
        this.estadcivil = estadcivil;
        this.estadolabo = estadolabo;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellpat() {
        return apellpat;
    }

    public void setApellpat(String apellpat) {
        this.apellpat = apellpat;
    }

    public String getApellmat() {
        return apellmat;
    }

    public void setApellmat(String apellmat) {
        this.apellmat = apellmat;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public int getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(int numdoc) {
        this.numdoc = numdoc;
    }

    public int getDianac() {
        return dianac;
    }

    public void setDianac(int dianac) {
        this.dianac = dianac;
    }

    public String getMesnac() {
        return mesnac;
    }

    public void setMesnac(String mesnac) {
        this.mesnac = mesnac;
    }

    public int getAnac() {
        return anac;
    }

    public void setAnac(int anac) {
        this.anac = anac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getRuc() {
        return ruc;
    }

    public void setRuc(int ruc) {
        this.ruc = ruc;
    }

    public String getEstadcivil() {
        return estadcivil;
    }

    public void setEstadcivil(String estadcivil) {
        this.estadcivil = estadcivil;
    }

    public String getEstadolabo() {
        return estadolabo;
    }

    public void setEstadolabo(String estadolabo) {
        this.estadolabo = estadolabo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idusuario != null ? idusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.idusuario == null && other.idusuario != null) || (this.idusuario != null && !this.idusuario.equals(other.idusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.empresa.proyecto.controller.Usuario[ idusuario=" + idusuario + " ]";
    }
    
}
