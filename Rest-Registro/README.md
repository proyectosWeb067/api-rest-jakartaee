# Build
mvn clean package && docker build -t com.empresa/Rest-Registro .

# RUN

docker rm -f Rest-Registro || true && docker run -d -p 8080:8080 -p 4848:4848 --name Rest-Registro com.empresa/Rest-Registro 