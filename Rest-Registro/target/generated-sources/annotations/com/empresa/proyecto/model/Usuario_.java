package com.empresa.proyecto.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-25T00:10:50")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> apellmat;
    public static volatile SingularAttribute<Usuario, Integer> ruc;
    public static volatile SingularAttribute<Usuario, Integer> anac;
    public static volatile SingularAttribute<Usuario, String> apellpat;
    public static volatile SingularAttribute<Usuario, String> estadolabo;
    public static volatile SingularAttribute<Usuario, Integer> numdoc;
    public static volatile SingularAttribute<Usuario, String> nombre;
    public static volatile SingularAttribute<Usuario, Integer> dianac;
    public static volatile SingularAttribute<Usuario, Integer> idusuario;
    public static volatile SingularAttribute<Usuario, String> estadcivil;
    public static volatile SingularAttribute<Usuario, String> mesnac;
    public static volatile SingularAttribute<Usuario, String> tipodoc;
    public static volatile SingularAttribute<Usuario, String> sexo;

}