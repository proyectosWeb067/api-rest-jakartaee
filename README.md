Descripcion
======================
Esta es una sencilla api-rest realizada con Maven ,jakartaEE y la especificacion  
JPA(java persistence api).

Se creo una pagina de ejemplo html con js y materialize como framework 
css, la cual permite consumir los servicios del servidor, en el momento 
de ser creado este servicio se consume de manera local, por lo cual se 
deberá instalar en el navegador un pugin llamado CORS, para que pueda 
funcionar nuestras peticiones.

La idea es consumir el servicio rest de las diferentes formas posibles, 
como practica, en primer lugar con el objeto XMLHttpRequest y solo con 
javascript traer los datos, posteriormente con ajax y con fetch; cada 
entrega tendrá sus respectivos html, js y css. 
---------------------------
Para probar de manera satisfactoria esta api en esta primera version, es necesario 
crear un pool de conexinones en el servidor de aplicaciones quien sera 
el encargado de gestionar la conexion a bases de datos de la api, 
sugiero utilizar payara server debido a que glassfish tiene algunos bugs en el momento de crear 
el pool de conexiones.


Pool de conexiones
------------------
Para el pool de conexiones en payara server es necesario descargar el driver del 
motor de base de datos que usted quiera utilizar, ya sea mysql,postgres,oracle 
etc, este driver se debe pegar en el directorio: domains/domains1/lib de
la carpeta de nuestro ervidor de aplicaciones en mi caso payara server; agregamos 
nuestro servidor a nuestro IDE en mi caso netbeans, y una vez agregado 
satisfactoriamente nuestro servidor abrimos el administrador web de nuestro 
servidor(click derecho sobre nuestro servidor agregado, en netbeans aparece en 
services-servers), ya dentro de el administrador de nuestro servidor de aplicaciones 
nos situamos en JDBC e ingresamos a connections pools creamos un nuevo pool y en este 
agregamos los datos de conexiona  a la base de datos(url, usuario, contraseña , etc), 
luego de estar creado nuesto pool nos dirijimos a JDBC-> resources en donde creamos 
el recurso jdni y le asociamos nuestro pool de conexiones anteriormente creado y con 
esto creamos nuestro pool.

Cliente Rest
----------------

El cliente rest que se utilizo fue postman, para realizar las peticiones 
al servicio.

Actualización
---------------
En princio el servicio rest se puede consumir desde el index.html pero 
para poder hacer esto es necesario que se tenga el plugin CORS activo.
esta forma la realize en primera medida para provar si era posible el 
consumo de datos, ahora he implementado un filtro en el lado del 
backend(java) que permite las peticiones HTTP ya sin necesidad del 
plugin en chrome.
